var Barrier = function(x, y, width, height, orientation, type, state) {
    var b = game.add.sprite(x + width / 2, y + height / 2, R.image.barrier);
    b.width = width;
    b.height = height;

    b.animations.add(R.anim.idle, [orientation + (type === "player" ? 2 : 0)], 1, false);
    b.animations.play(R.anim.idle);

    game.physics.p2.enable(b);
    b.body.static = true;

    b.body.clearShapes();
    b.body.addRectangle((orientation ? 18 : width), (orientation ? height : 18), 0, 0);

    if (type === "box") {
        b.body.setCollisionGroup(state.coll_barr_box);
        b.body.collides(state.coll_objects);
    } else if (type === "player") {
        b.body.setCollisionGroup(state.coll_barr_player);
        b.body.collides(state.coll_player);
    }

};