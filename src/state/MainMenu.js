var MainMenu = function() {};
MainMenu.prototype = {
    create: function() {
        this.title = game.add.sprite(0, 0, R.image.title);

        this.textStartGame = game.add.bitmapText(1050, 250, R.font.mario, "Start Game", 40);
        this.textStartGame.anchor.setTo(.5, .5);

        this.textTween = game.add.tween(this.textStartGame).to({ fontSize: 48 }, 750, Phaser.Easing.Linear.None).to({ fontSize: 40 }, 750, Phaser.Easing.Linear.None).loop();

        this.chkbx_mp = new Checkbox(1050, 350, "Local MP", function(ticked) {
            lastMapInfo.local_mp = ticked;
        });

        this.sldr_text = new Slider(1050, 430, 250, {
            start: "quite quiet",
            end: "loud af"
        }, {
            min: 0,
            max: 100,
            start: 75
        }, function(value) {
            console.info(value);
        });

        lastMapInfo.next_map = "map_1";
    },
    update: function() {
        var intersect1 = this.textStartGame.getBounds().contains(game.input.x, game.input.y);
        if (intersect1 && !this.textTween.isPaused) {
            this.textTween.start();
        } else if (!intersect1 && this.textTween.isRunning) {
            this.textTween.stop();
            this.textTween = game.add.tween(this.textStartGame).to({ fontSize: 48 }, 750, Phaser.Easing.Linear.None).to({ fontSize: 40 }, 750, Phaser.Easing.Linear.None).loop();

            game.add.tween(this.textStartGame).to({ fontSize: 40}, 750, Phaser.Easing.Linear.None).start();
        }

        if (intersect1 && game.input.activePointer.leftButton.isDown)
            game.state.start(R.state.maingame);

        this.chkbx_mp.update();
        this.sldr_text.update();
    }
};
