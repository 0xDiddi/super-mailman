var Fire = function(x, y, parent) {
    //todo: attach emitters to the parent in a proper way
    var emitter = game.add.emitter(x, y, 500);
    emitter.makeParticles(R.image.fireball);
    emitter.gravity = -400;
    //parent.addChild(emitter);

    var burst = game.add.emitter(x, y, 500);
    burst.makeParticles(R.image.fireball);
    burst.gravity = -400;
    //parent.addChild(burst);

    burst.start(true, 1500, 0, 100);
    //emitter.start(false, 750, 25, -1);

    var destroy = function() {
        emitter.destroy();
        burst.destroy();
    };

    game.time.events.add(7500, destroy, this);
};

var AddMedals = function(x, y, num) {
    var medals = [];

    for (var i = 0; i < num; i++)
        medals.push(game.rnd.integerInRange(0,8));

    Medals = Medals.concat(medals);

    var emitter = game.add.emitter(x, y, num);
    emitter.gravity = 400;
    emitter.makeParticles(R.image.medals, medals);
    emitter.start(true, 0, 0, num);

    var sfx = game.add.audio(R.audio.medals);
    sfx.play();
};

var Die = function(s) {
    if (s.isDead) return;

    s.body.setCollisionGroup(game.physics.p2.nothingCollisionGroup);
    s.body.velocity.x = 0;
    s.body.velocity.y = -300;

    s.isDead = true;
};

var createInputCursorForPlayer = function(player) {
    var cursor;
    if (player === 1) {
        cursor = {
            up: game.input.keyboard.addKey(Phaser.Keyboard.W),
            down: game.input.keyboard.addKey(Phaser.Keyboard.S),
            left: game.input.keyboard.addKey(Phaser.Keyboard.A),
            right: game.input.keyboard.addKey(Phaser.Keyboard.D),
            action: game.input.keyboard.addKey(Phaser.Keyboard.E)
        };
    } else if (player === 2) {
        cursor = game.input.keyboard.createCursorKeys();
        cursor.action = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
    }
    return cursor;
};